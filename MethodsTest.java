public class MethodsTest
{
	public static void main (String[]args)
	{
	 int x = 5;
	 System.out.println(x);
	 methodNoInputNoReturn();
	 System.out.println(x);
	 
	 methodOneInputNoReturn(x+10);
	 System.out.println(x);
	 
	 methodTwoInputNoReturn(5,10.5);
	 
	int a = methodNoInputReturnInt();
	System.out.println(a);
	
	double squaredSum=sumSquareRoot(9, 5);
	System.out.println(squaredSum);
	
	String s1= "java";
	String s2= "programming";
	System.out.println(s1.length());
	System.out.println(s2.length());
	
	System.out.println(SecondClass.addOne(50));

	SecondClass sc= new SecondClass();
	System.out.println(sc.addTwo(50));
	


	}
	
	public static void methodNoInputNoReturn()
	{
	System.out.println("I’m in a method that takes no input and returns nothing");
	int x=20;
	System.out.println(x);
	}

	public static void methodOneInputNoReturn(int x)
	{
		x-=5;
	System.out.println(x);
	}
	
	public static void methodTwoInputNoReturn(int x, double y)
	{
	System.out.println("int "+x);
	System.out.println("double "+y);
	}
	
	public static int methodNoInputReturnInt()
	{
	int x = 5;
	return x;
	}
	
	public static double sumSquareRoot(int num1, int num2)
	{
		double sum=num1+num2; 
		return Math.sqrt(sum);
	}
}